# pcap-parser-final

This is the solution to code exercise from tsurucapital: http://www.tsurucapital.com/en/code-sample.html

## Usage
stack exec parse-quote -- {-flag} {filepath}

## Build
```
stack build --fast
stack exec parse-quote -- -r mdf-kospi200.20110216-0.pcap > result.txt 
```



## Resource on understaind pcap file

https://wiki.wireshark.org/Development/LibpcapFileFormat
