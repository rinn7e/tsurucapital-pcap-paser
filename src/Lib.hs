module Lib where

import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as B
import Data.Binary.Get
import System.IO
import System.Environment
import qualified Data.ByteString.Char8 as C
import qualified Data.List as DL
  
import Data.Word  
import Data.Int
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)

data GHeader = GHeader 
  { magicNumber :: Word32 
  , versionMajor :: Word16 
  , versionMinor :: Word16 
  , thiszone :: Int32 
  , sigfigs :: Word32 
  , snaplen :: Word32 
  , network :: Word32
  } deriving Show

data PHeader = PHeader 
  { tsSec :: Word32
  , tsUsec :: Word32
  , inclLen :: Word32
  , orignLen :: Word32
  } deriving Show

data PData = PData 
  { quote :: B.ByteString
  , issueCode :: B.ByteString

  , bestBitPrice1 :: B.ByteString
  , bestBitQty1 :: B.ByteString
  , bestBitPrice2 :: B.ByteString
  , bestBitQty2 :: B.ByteString
  , bestBitPrice3 :: B.ByteString
  , bestBitQty3 :: B.ByteString
  , bestBitPrice4 :: B.ByteString
  , bestBitQty4 :: B.ByteString
  , bestBitPrice5 :: B.ByteString
  , bestBitQty5 :: B.ByteString

  , bestAskPrice1 :: B.ByteString
  , bestAskQty1 :: B.ByteString
  , bestAskPrice2 :: B.ByteString
  , bestAskQty2 :: B.ByteString
  , bestAskPrice3 :: B.ByteString
  , bestAskQty3 :: B.ByteString
  , bestAskPrice4 :: B.ByteString
  , bestAskQty4 :: B.ByteString
  , bestAskPrice5 :: B.ByteString
  , bestAskQty5 :: B.ByteString
  
  , acceptTime :: B.ByteString
  , seconds :: Int
  } deriving (Show, Eq)

getGHeader :: BL.ByteString -> (GHeader, BL.ByteString)
getGHeader bytes = 
  let (head, tail) = BL.splitAt 24 bytes
  in 
    runGet ( do
      magicNumber   <- getWord32le
      versionMajor  <- getWord16le
      versionMinor  <- getWord16le  
      thiszone      <- getInt32le 
      sigfigs       <- getWord32le 
      snaplen       <- getWord32le 
      network       <- getWord32le 
      return (GHeader magicNumber versionMajor versionMinor thiszone sigfigs snaplen network, tail)
    ) head

getPacket :: BL.ByteString -> (Maybe (PHeader, PData), BL.ByteString)
getPacket bytes =  
  let 
    (pheader, tail) = getPHeader bytes
    (bodyBytes, tail2) = BL.splitAt (fromIntegral $ inclLen $ pheader :: Int64) tail
    maybePdata = getPData (BL.drop 42 bodyBytes)
  in
    case maybePdata of 
      Nothing -> 
        (Nothing, tail2)
      Just pdata -> 
        (Just (pheader, pdata) , tail2)

getPHeader :: BL.ByteString -> (PHeader, BL.ByteString)
getPHeader bytes = 
  let (head, tail) = BL.splitAt 16 bytes
  in 
    runGet ( do
      tsSec <- getWord32le
      tsUsec <- getWord32le
      inclLen <- getWord32le
      orignLen <- getWord32le
      return (PHeader tsSec tsUsec inclLen orignLen, tail)
    ) head

getPData ::  BL.ByteString -> Maybe PData
getPData bodyBytes = 
  if (BL.length bodyBytes < 5) then Nothing
  else if (BL.take 5 bodyBytes) /= "B6034" then Nothing
  else 
    runGet ( do
      quote <- getByteString 5
      issueCode <- getByteString 12
      skip 12
      bestBitPrice1 <- getByteString 5
      bestBitQty1 <- getByteString 7
      bestBitPrice2 <- getByteString 5
      bestBitQty2 <- getByteString 7
      bestBitPrice3 <- getByteString 5
      bestBitQty3 <- getByteString 7
      bestBitPrice4 <- getByteString 5
      bestBitQty4 <- getByteString 7
      bestBitPrice5 <- getByteString 5
      bestBitQty5 <- getByteString 7
      skip 7
      bestAskPrice1 <- getByteString 5
      bestAskQty1 <- getByteString 7
      bestAskPrice2 <- getByteString 5
      bestAskQty2 <- getByteString 7
      bestAskPrice3 <- getByteString 5
      bestAskQty3 <- getByteString 7
      bestAskPrice4 <- getByteString 5
      bestAskQty4 <- getByteString 7
      bestAskPrice5 <- getByteString 5
      bestAskQty5 <- getByteString 7
      skip 50
      acceptTime <- getByteString 8

      let (hour :: Int) = read $ C.unpack $ B.take 2 acceptTime
      let (minute :: Int) = read $ C.unpack $ B.take 2 $ B.drop 2 acceptTime
      let (second :: Int) = read $ C.unpack $ B.take 2 $ B.drop 4 acceptTime

      return $ Just
        ( PData quote issueCode  
            bestBitPrice1 bestBitQty1 bestBitPrice2 bestBitQty2 bestBitPrice3 bestBitQty3 bestBitPrice4 bestBitQty4 bestBitPrice5 bestBitQty5
            bestAskPrice1 bestAskQty1 bestAskPrice2 bestAskQty2 bestAskPrice3 bestAskQty3 bestAskPrice4 bestAskQty4 bestAskPrice5 bestAskQty5
            acceptTime (hour * 3600 + minute * 60 + second)
        )
    ) bodyBytes

parsePacket :: Bool -> BL.ByteString -> [(PHeader, PData)]
parsePacket isSort bytes = 
  parsePacket' bytes
  where
    parsePacket' bytes = 
      case BL.null bytes of 
        True -> []
        False -> 
          let
            (maybePacket, tail) = getPacket bytes
          in
            case maybePacket of
              Nothing ->
                parsePacket' tail
              Just packet -> 
                packet : (parsePacket' tail)


runSort :: [(PHeader, PData)] -> [(PHeader, PData)]
runSort packetList = 
  DL.sortOn (\(ph, pd) -> seconds pd) packetList

prettyPrint :: [(PHeader, PData)] -> IO ()
prettyPrint packets = do 
  putStrLn "<pkt-time> <accept-time> <issue-code> <bqty5>@<bprice5> <bqty4>@<bprice4> <bqty3>@<bprice3> <bqty2>@<bprice2> <bqty1>@<bprice1> <aqty1>@<aprice1> <aqty2>@<aprice2> <aqty3>@<aprice3> <aqty4>@<aprice4> <aqty5>@<aprice5>"
  -- putStrLn "1297814400 08595997      KR4201F32705 0000000@00000     0000000@00000     0000000@00000     0000000@00000     0000000@00000     0000000@00000     0000000@00000     0000000@00000     0000000@00000     0000000@00000    "
  putStrLn $ DL.intercalate "\n" $ fmap (\(h, b) ->
      (show $ tsSec h)            <> " "      <> 
      (C.unpack $ acceptTime b)   <> "      " <>
      (C.unpack $ issueCode b)    <> " "      <>
      (C.unpack $ bestBitQty5 b)  <> "@" <> (C.unpack $ bestBitPrice5 b)  <> "     " <>
      (C.unpack $ bestBitQty4 b)  <> "@" <> (C.unpack $ bestBitPrice4 b)  <> "     " <>
      (C.unpack $ bestBitQty3 b)  <> "@" <> (C.unpack $ bestBitPrice3 b)  <> "     " <>
      (C.unpack $ bestBitQty2 b)  <> "@" <> (C.unpack $ bestBitPrice2 b)  <> "     " <>
      (C.unpack $ bestBitQty1 b)  <> "@" <> (C.unpack $ bestBitPrice1 b)  <> "     " <>
      (C.unpack $ bestAskQty1 b)  <> "@" <> (C.unpack $ bestAskPrice1 b)  <> "     " <>
      (C.unpack $ bestAskQty2 b)  <> "@" <> (C.unpack $ bestAskPrice2 b)  <> "     " <>
      (C.unpack $ bestAskQty3 b)  <> "@" <> (C.unpack $ bestAskPrice3 b)  <> "     " <>
      (C.unpack $ bestAskQty4 b)  <> "@" <> (C.unpack $ bestAskPrice4 b)  <> "     " <>
      (C.unpack $ bestAskQty5 b)  <> "@" <> (C.unpack $ bestAskPrice5 b)  <> "    "
    ) packets

run = do
  args <- getArgs
  case length args of
    1 -> parsePcap False (head args)
    2 -> case args of
      ("-r":filePath:[]) -> parsePcap True filePath
      (filePath:"-r":[]) -> parsePcap True filePath
      _ -> error "Incorrect argument"
    _ -> error "Incorrect argument provided. Need pcap filename, and optional parameter '-r'"

    where
      parsePcap :: Bool -> String -> IO ()
      parsePcap isSort filePath = 
        withFile filePath ReadMode $ \h -> do
          bytes <- BL.hGetContents h

          -- print $ "Sort Flag: " <> show isSort
          
          let (header, rest) = getGHeader bytes
          let packetList = parsePacket False rest
          
          prettyPrint $ (if isSort then runSort packetList else packetList)